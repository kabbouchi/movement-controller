﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

/* Debug class using reflection to access private fields */

public class WeaponInfo : MonoBehaviour
{
    public BaseWeapon wep;
    public Text bulletCount;

    private void Update()
    {
        bulletCount.text = wep.GetBulletCount.ToString() + " / " + wep.GetMaxBullet.ToString();
    }
}
