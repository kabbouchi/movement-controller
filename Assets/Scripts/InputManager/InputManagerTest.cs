﻿#if true
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputManagerTest : MonoBehaviour
{
    public Slider leftJoystickX, leftJoystickY;
    public Slider rightJoystickX, rightJoystickY;

    public Transform aButton, bButton, xButton, yButton;

    private void Update()
    {
        // Left Joystick
        leftJoystickX.value = InputManager.LeftHorizontal();
        leftJoystickY.value = InputManager.LeftVertical();

        // Right Joystick
        rightJoystickX.value = InputManager.RightHorizontal();
        rightJoystickY.value = InputManager.RightVertical();

        // Primary Fire
        aButton.GetChild(0).gameObject.SetActive(InputManager.IsDown(InputName.PrimaryFire));
        aButton.GetChild(1).gameObject.SetActive(InputManager.IsHeld(InputName.PrimaryFire));
        aButton.GetChild(2).gameObject.SetActive(InputManager.IsUp(InputName.PrimaryFire));

        // Secondary Fire
        bButton.GetChild(0).gameObject.SetActive(InputManager.IsDown(InputName.SecondaryFire));
        bButton.GetChild(1).gameObject.SetActive(InputManager.IsHeld(InputName.SecondaryFire));
        bButton.GetChild(2).gameObject.SetActive(InputManager.IsUp(InputName.SecondaryFire));

        // X Button
        xButton.GetChild(0).gameObject.SetActive(InputManager.IsDown(InputName.Ability1));
        xButton.GetChild(1).gameObject.SetActive(InputManager.IsHeld(InputName.Ability1));
        xButton.GetChild(2).gameObject.SetActive(InputManager.IsUp(InputName.Ability1));

        // Y Button
        yButton.GetChild(0).gameObject.SetActive(InputManager.IsDown(InputName.Ability2));
        yButton.GetChild(1).gameObject.SetActive(InputManager.IsHeld(InputName.Ability2));
        yButton.GetChild(2).gameObject.SetActive(InputManager.IsUp(InputName.Ability2));
    }
}
#endif